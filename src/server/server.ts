import { Server } from "@hapi/hapi";

export const ptts = new Server({
    address: "0.0.0.0",
    port: process.env.PORT || 80,
});
