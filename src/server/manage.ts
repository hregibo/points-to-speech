import { ServerRoute, Request } from "@hapi/hapi";
import Boom from "@hapi/boom";
import jwt from "jsonwebtoken";
import { getClient } from "../utils/database";
import RewardQueries from "../utils/rewards.queries";
import { PoolClient } from "pg";
import { logger } from "../utils/logging";

export const updateActionReward: ServerRoute = {
    method: 'PUT',
    path: '/actions/{uuid}',
    handler: async (request, helper) => {
        //
    },
};
export const deleteActionReward: ServerRoute = {
    method: 'DELETE',
    path: '/actions/{uuid}',
    handler: async (request, helper) => {
        //
    },
};
export const getActionReward: ServerRoute = {
    method: 'GET',
    path: '/actions/{uuid}',
    handler: async (request, helper) => {
        //
    },
};
export const listActionRewards: ServerRoute = {
    method: 'GET',
    path: '/actions',
    handler: async (request, helper) => {
        //
    },
};
export const addActionReward: ServerRoute = {
    method: 'POST',
    path: '/actions',
    handler: async (request, helper) => {
        //
    },
};


export const addTemplate: ServerRoute = {
    method: 'POST',
    path: '/templates',
    handler: async (request, helper) => {
        //
    },
};

export const deleteTemplate: ServerRoute = {
    method: 'DELETE',
    path: '/templates/{uuid}',
    handler: async (request, helper) => {
        //
    },
};

export const getTemplate: ServerRoute = {
    method: 'GET',
    path: '/templates/{uuid}',
    handler: async (request, helper) => {
        const channelId = await validateToken(request);
        const templateId = request.params.uuid;
        let client: PoolClient|undefined;
        try {
            client = await getClient();
            const templates = await client.query(RewardQueries.getTemplate, [channelId, templateId]);
            if (templates.rowCount !== 1) {
                return Boom.notFound();
            }
            return helper.response(templates.rows[0]);
        } catch (err) {
            logger.error(err, "Failed to request templates");
            return Boom.internal();
        } finally {
            if (client) { client.release(); }
        }
    },
};

export const validateToken = async (request: Request): Promise<string> => {
    if (!request.headers.authorization) {
        throw Boom.unauthorized("Requests must be authenticated");
    }
    try {
        const authenticationToken = request.headers.authorization.substr("JWT ".length);
        const parsedToken = jwt.verify(authenticationToken, process.env.JWT_SECRET || 'dev', {
            ignoreExpiration: false,
            audience: 'effeebot.lumikko.dev',
            ignoreNotBefore: false,
        }) as object;
        return Reflect.get(parsedToken, 'channel') as string || '';
    } catch (failure) {
        throw Boom.unauthorized("Authentication token is expired");
    }
}

export const listTemplates: ServerRoute = {
    method: 'GET',
    path: '/templates',
    handler: async (request, helper) => {
        const channelId = await validateToken(request);
        let client: PoolClient|undefined;
        try {
            client = await getClient();
            const templates = await client.query(RewardQueries.listTemplates, [channelId]);
            if (templates.rowCount === 0) {
                return helper.response({
                    total: 0,
                    data: [],
                });
            }
            return helper.response({
                total: templates.rowCount,
                data: templates.rows,
            });
        } catch (err) {
            logger.error(err, "Failed to request templates");
            return Boom.internal();
        } finally {
            if (client) { client.release(); }
        }
    },
};

export default [
    // add
    addActionReward,
    addTemplate,
    // delete
    deleteActionReward,
    deleteTemplate,
    // get
    getActionReward,
    getTemplate,
    // list
    listActionRewards,
    listTemplates,
];
