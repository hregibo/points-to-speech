import { ServerRoute } from "@hapi/hapi";
import { PoolClient } from "pg";
import superAgent from "superagent";
import { v4 as createUUID } from "uuid";

import { logger } from "../utils/logging";
import { getClient } from "../utils/database";

import * as csrfQueries from "../utils/csrf.queries";
import * as tokenQueries from "../utils/tokens.queries";

export const scopes = [
    "channel:read:redemptions",
    "chat:edit",
];

export const authenticate: ServerRoute = {
    handler: async (request, helper) => {
        // let redirectAfter: string;
        // if (request.query.redirect_after) {
        //     redirectAfter = Array.isArray(request.query.redirect_after) 
        //     ? request.query.redirect_after.join('') 
        //     : request.query.redirect_after;
        // }
        const state = createUUID();
        let client: PoolClient|undefined;
        try {
            client = await getClient();
            await client.query(csrfQueries.saveAuthenticationCSRF, [ state ]);
        } catch ( error ) {
            logger.error({
                error,
            }, "Failed to save CSRF");
            return helper.response({
                message: "Failed to save your authentication token, please try again.",
            }).code(500);
        } finally {
            if (client) {
                client.release();
            }
        }
        return helper.redirect(`https://id.twitch.tv/oauth2/authorize?client_id=${
            process.env.OAUTH_CLIENT
        }&redirect_uri=${
            encodeURIComponent(process.env.OAUTH_REDIRECT || "http://localhost/callback")
        }&response_type=code&scope=${
            scopes.map((s) => encodeURIComponent(s)).join("+")
        }&state=${ state }`);
    },
    method: "GET",
    path: "/authenticate",
};

export const refreshToken = async (token: string) => {
    const validity = await superAgent.post(`https://id.twitch.tv/oauth2/token?grant_type=refresh_token&refresh_token=${
        token
    }&client_id=${
        process.env.OAUTH_CLIENT
    }&client_secret=${
        process.env.OAUTH_SECRET
    }`);
    return validity.body;
};

export const validateCode = async (code: string) => {
    const validity = await superAgent.post(`https://id.twitch.tv/oauth2/token?client_id=${
        process.env.OAUTH_CLIENT
    }&client_secret=${
        process.env.OAUTH_SECRET
    }&code=${code}&grant_type=authorization_code&redirect_uri=${
        process.env.OAUTH_REDIRECT
    }`);
    if (!validity.body || Object.keys(validity.body).length === 0) {
        throw Error("No body was found in the response from Twitch");
    }
    return {
        accessToken: validity.body.access_token,
        expiresIn: validity.body.expires_in,
        refreshToken: validity.body.refresh_token,
        scopes: validity.body.scope,
        type: validity.body.token_type,
    };
};

export const validateToken = async (accessToken: string) => {
    const tokenData = await superAgent.get("https://id.twitch.tv/oauth2/validate")
        .set("Authorization", `OAuth ${accessToken}`);
    if (!tokenData.body || Object.keys(tokenData.body).length === 0) {
        throw Error("Error while querying the token validation from Twitch APIs");
    }
    return tokenData.body;
};

export const callback: ServerRoute = {
    handler: async (request, helper) => {
        // Request validity of code to Twitch Server
        if (!request.query.code) {
            // do something, we should have a code
            return helper.response({
                message: "Missing code query parameter",
            }).code(400);
        }
        if (!request.query.state) {
            return helper.response({
                message: "Missing csrf token, please reconnect",
            }).code(400);
        }
        const code = Array.isArray(request.query.code) ? request.query.code.join(":") : request.query.code;
        const csrf = Array.isArray(request.query.state) ? request.query.state.join("") : request.query.state;
        let client: PoolClient|undefined;
        try {
            client = await getClient();
            const foundCSRF = await client.query(csrfQueries.findAuthenticationCSRF, [csrf]);
            if (foundCSRF.rowCount === 0) {
                return helper.response({
                    message: "Invalid CSRF token",
                }).code(400);
            }
            await client.query(csrfQueries.expireAuthenticationCSRF, [csrf]);
        } catch (error) {
            logger.error({ error }, "Failed to validate/remove CSRF token from database");
            return helper.response({
                message: "Internal Error, please try again later",
            }).code(500);
        } finally {
            if (client) {
                client.release();
            }
        }
        let tokenInfo;
        let tokenValidation;
        try {
            tokenInfo = await validateCode(code);
            tokenValidation = await validateToken(tokenInfo.accessToken);
        } catch (xhrError) {
            return helper.response({
                message: "An error occured while validating your request. Please re-authenticate.",
                xhrError,
            }).code(400);
        }

        if (!["lumikkode", "effee"].includes(tokenValidation.login)) {
            return helper.response({
                message: "Sorry but only Effee QT & Lumikkode can authenticate using this page.",
            }).code(401);
        }

        try {
            client = await getClient();
            const expiryDate = new Date();
            expiryDate.setSeconds(expiryDate.getSeconds() + tokenValidation.expires_in);
            const saveStatus = await client.query(tokenQueries.addToken, [
                tokenValidation.user_id,
                tokenValidation.login,
                tokenInfo.accessToken,
                tokenInfo.refreshToken,
                expiryDate.toISOString(),
            ]);
            return helper.response({
                message: "ok",
            }).code(200);
        } catch (error) {
            logger.error({ error, user: {
                id: tokenValidation.user_id,
                login: tokenValidation.login,
            } }, "Failed to save into database the token for user");
            return helper.response({
                message: "Error while saving your authentication token, please try again.",
            }).code(500);
        } finally {
            if (client) {
                client.release();
            }
        }
    },
    method: "GET",
    path: "/callback",
};

export default [
    authenticate,
    callback,
];
