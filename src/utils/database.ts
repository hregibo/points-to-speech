import { Pool, PoolClient } from "pg";

export const connectionPool: Pool = new Pool({
    connectionTimeoutMillis: 5000,
    database: process.env.DB_NAME || "ptts",
    host: process.env.DB_HOST || "127.0.0.1",
    password: process.env.DB_PASS || "ptts",
    port: Number(process.env.DB_PORT) || 5432,
    user: process.env.DB_USER || "ptts",
});

export const getClient = async (): Promise<PoolClient> => {
    return await connectionPool.connect();
};
