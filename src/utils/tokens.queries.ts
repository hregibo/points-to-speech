export const createTokensTable = `CREATE TABLE IF NOT EXISTS tokens (
    id VARCHAR(64) NOT NULL PRIMARY KEY,
    login VARCHAR(128) DEFAULT NULL,
    access_token VARCHAR(64),
    refresh_token VARCHAR(64),
    expiry TIMESTAMPTZ
)`;

export const addToken = `INSERT INTO tokens (id, login, access_token, refresh_token, expiry)
VALUES ($1, $2, $3, $4, $5)
ON CONFLICT (id)
DO UPDATE SET
    login = EXCLUDED.login,
    access_token = EXCLUDED.access_token,
    refresh_token = EXCLUDED.refresh_token,
    expiry = EXCLUDED.expiry
`;

export const getTokens = `SELECT id, access_token, refresh_token, expiry FROM tokens`;

export const updateToken = `UPDATE tokens SET access_token = $1, refresh_token = $2, expiry = $3 WHERE id = $4`;

export const tokenChangeListener = 'LISTEN token_change';
export const createNotifyFunction = `CREATE OR REPLACE FUNCTION notify_token_change () RETURNS TRIGGER AS $$
DECLARE
BEGIN
    PERFORM pg_notify('token_change', row_to_json(NEW)::text);
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;`;
export const createNotifyTrigger = `DROP TRIGGER IF EXISTS tokens_events_change ON tokens;
CREATE TRIGGER tokens_events_change
    AFTER INSERT OR UPDATE ON tokens
    FOR EACH ROW
    EXECUTE PROCEDURE notify_token_change();`;
