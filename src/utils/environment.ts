import { logger } from "./logging";

const requiredEnv = [
    "DB_NAME",
    "DB_HOST",
    "DB_USER",
    "DB_PASS",
    "OAUTH_CLIENT",
    "OAUTH_SECRET",
    "OAUTH_REDIRECT",
];

for (const envKey of requiredEnv) {
    if ( !Reflect.has(process.env, envKey) ) {
        logger.fatal({
            envKey,
        }, "Missing mandatory environment variable");
        process.exit(1);
    }
}
