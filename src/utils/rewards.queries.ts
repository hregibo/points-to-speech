export const createActionsTable = `CREATE TABLE IF NOT EXISTS actions (
    id SERIAL PRIMARY KEY,
    reward VARCHAR(64) NOT NULL,
    message VARCHAR(512) NOT NULL,
    description VARCHAR(128) DEFAULT '',
)`;
export const createTemplatesTable = `CREATE TABLE IF NOT EXISTS templates (
    uuid VARCHAR(64) NOT NULL PRIMARY KEY,
    channel VARCHAR(128) NOT NULL,
    name VARCHAR(256),
    icon VARCHAR(512),
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP
);`;

export const addTemplate = `INSERT INTO templates (uuid, channel, name, icon) VALUES ($1, $2, $3, $4);`;
export const deleteTemplate = `DELETE FROM templates WHERE uuid = $1;`;
export const getTemplate = `SELECT uuid, channel, name, icon FROM templates WHERE channel = $1 AND uuid = $2;`;
export const listTemplates = `SELECT uuid, channel, name, icon FROM templates WHERE channel = $1;`;

export const addAction = `INSERT INTO actions (reward, message, description) VALUES ($1, $2, $3);`;
export const deleteAction = `DELETE FROM actions WHERE id = $1;`;
export const getAction = `SELECT id, reward, message, description FROM actions WHERE id = $1;`;
export const listActions = `SELECT id, reward, message, description FROM actions;`;
export const updateAction = `UPDATE actions SET message = $2, description = $3 WHERE id = $1;`;

export default {
    // add
    addAction,
    addTemplate,
    // create
    createActionsTable,
    createTemplatesTable,
    // delete
    deleteAction,
    deleteTemplate,
    // get
    getAction,
    getTemplate,
    // list
    listActions,
    listTemplates,
    // update
    updateAction,
}