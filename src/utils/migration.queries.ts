export const createMigrationTable = `CREATE TABLE IF NOT EXISTS migrations (
    id SERIAL PRIMARY KEY,
    migration_name VARCHAR(128) NOT NULL,
    migrated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP
);`;

export const insertMigration = `INSERT INTO migrations (migration_name) VALUES ($1);`;
export const findMigration = `SELECT id, migration_name, migrated_at FROM migrations WHERE migration_name = $2;`;
