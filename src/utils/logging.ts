import { createLogger } from "bunyan";

export const logger = createLogger({
    level: process.env.NODE_ENV === "production" ? "info" : "debug",
    name: "ptts",
    stream: process.stdout,
});
