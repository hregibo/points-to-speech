export const saveAuthenticationCSRF = `INSERT INTO csrf_tokens (state) VALUES ($1)`;

export const findAuthenticationCSRF = `SELECT state, created_at FROM csrf_tokens WHERE state = $1 LIMIT 1`;

export const expireAuthenticationCSRF = `DELETE FROM csrf_tokens WHERE state = $1`;

export const createCSRFTable = `CREATE TABLE IF NOT EXISTS csrf_tokens (
    state VARCHAR(64) NOT NULL PRIMARY KEY,
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP
)`;
