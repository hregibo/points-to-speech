import { PoolClient } from "pg";
import { v4 } from "uuid";
import { client, connection } from "websocket";
import { refreshToken, validateToken } from "../server/authentication";
import { getClient } from "../utils/database";
import * as tokenQueries from "../utils/tokens.queries";
import { logger } from "../utils/logging";

export interface IPubSubClient {
    socket?: connection;
    reconnectDelay: number;
    topics: string[];
    connectedSince?: string;
    connect: () => Promise<void>;
    disconnect: () => void;
    pingInterval?: NodeJS.Timeout;
}

export const send = (socket: connection, message: string): void => {
    if (socket) { socket.sendUTF(message); }
    logger.debug({
        message,
    }, "sent message to pubsub");
};

// TODO: on 'RECONNECT', do a incremental reconnect

export const create = (): IPubSubClient => {
    const socket = new client();
    const psc: IPubSubClient = {
        connect: (): Promise<void> => {
            return new Promise((resolve, reject) => {
                socket.on("connect", (conn: connection) => {
                    logger.info("connected to pub-sub server");
                    psc.socket = conn;
                    psc.connectedSince = new Date().toISOString();
                    sendPing(psc);
                    resolve();
                });
                socket.on("connectFailed", reject);
                socket.connect(process.env.PUBSUB_SERVER_URI || "wss://localhost:8081");
            });
        },
        disconnect: () => {
            if (!psc.socket || !psc.socket.connected) {
                return;
            }
            psc.socket.close();
            psc.connectedSince = undefined;
            if (psc.pingInterval) {
                clearTimeout(psc.pingInterval);
            }
        },
        reconnectDelay: 5000,
        topics: [],
    };
    return psc;
};

export const sendPing = (pubSubClient: IPubSubClient) => {
    if (pubSubClient.socket && pubSubClient.socket.connected) {
        const payload = {
            type: "PING",
        };
        pubSubClient.socket.sendUTF(JSON.stringify(payload));
        pubSubClient.pingInterval = setTimeout(() => sendPing(pubSubClient), 60000);
        pubSubClient.pingInterval.unref();
    }
};

export interface ITopicData {
    auth_token?: string;
    topics: string[];
}

export const addTopic = (pubSubClient: IPubSubClient, data: ITopicData) => {
    if (!pubSubClient.socket) {
        return;
    }
    pubSubClient.topics.push(...data.topics);
    const nonce = v4();
    send(pubSubClient.socket, JSON.stringify({
        data,
        nonce,
        type: "LISTEN",
    }));
};

export interface IUserToken {
    access_token: string;
    expiry: string;
    refresh_token: string;
    user_id: string;
}

export const listen = async (userTokens: IUserToken[]) => {
    // loop through the tokens, check expiry & revalidate if < 2h
    const validTokens = [];
    for (const token of userTokens) {
        let dbh: PoolClient|undefined;
        try {
            const newToken = await refreshToken(token.refresh_token);
            const expiry = new Date();
            expiry.setSeconds(expiry.getSeconds() + newToken.expires_in);
            dbh = await getClient();
            await dbh.query(tokenQueries.updateToken, [
                newToken.access_token,
                newToken.refresh_token,
                expiry.toISOString(),
                token.user_id,
            ]);
            token.expiry = newToken.expires_in;
            token.access_token = newToken.access_token;
            token.refresh_token = newToken.refresh_token;
            await validateToken(token.access_token);
            validTokens.push(token);
        } catch (err) {
            logger.error("Failed to update the token", { token, err });
        } finally {
            if (dbh) { dbh.release(); }
        }
    }
    // create the pubsub
    if (validTokens.length === 0) {
        return null;
    }
    const psc = await create();
    await psc.connect();
    if (psc.socket) {
        psc.socket.on("message", ({utf8Data: data}) => {
            logger.info({
                message: data,
            }, "recieved data from PubSub Server");
        });
        psc.socket.on("error", logger.error);
        psc.socket.on("close", (n,d) => logger.error("closed socket"))
    }
    // add topics
    for (const token of validTokens) {
        addTopic(psc, {
            auth_token: token.access_token,
            topics: [`channel-points-channel-v1.${token.user_id}`],
        });
    }
    // return pubsub
    return psc;
};

export default {
    create,
    send,
    sendPing,
};
