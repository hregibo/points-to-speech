// import "./utils/environment";
import { getClient } from "../utils/database";
import { tokenChangeListener } from "../utils/tokens.queries";

import { PoolClient } from "pg";
import { logger } from "../utils/logging";
import { create } from "./pub-sub-client";

let _notifyAlert: PoolClient|undefined;

(async () => {
    try {
        // create pubsub client
        const psc = create();

        // create listener to PGSQL trigger
        _notifyAlert = await getClient();
        await _notifyAlert?.query(tokenChangeListener);
        _notifyAlert.on('notification', (msg) => {
            logger.info(msg, "Received message from PGSQL");
            // verify we have the token already
            // if we do, first send a UNLISTEN as auth changed
            // then, regardless of if we had it or not, LISTEN with new auth
        });

        // connect to the PubSub if we have at least one LISTEN available
        await psc.connect();
        // disconnect from the pubsub if we have no more LISTEN available
        // if the Pubsub sends us a message from a channel point redeem
        // check if already in the rewards or not, and if not insert it
        // insert the transaction into the channelpoints_transaction database
        // this will trigger the listener of the IRC Client which will send the data
    } catch (error) {
        logger.fatal({
            error,
        }, "Failed to migrate tables, exiting microservice");
    }
})();
