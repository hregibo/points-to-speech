// import "./utils/environment";
import { ptts } from "./server/server";

import authRoutes from "./server/authentication";
import manageRoutes from "./server/manage";
import { getClient } from "./utils/database";

import { createCSRFTable } from "./utils/csrf.queries";
import { createTokensTable, getTokens, createNotifyFunction, createNotifyTrigger } from "./utils/tokens.queries";

import { PoolClient } from "pg";
import { logger } from "./utils/logging";
import { listen } from "./pub-sub-client/pub-sub-client";

const startServer = async () => {
    let client: PoolClient|undefined;
    try {
        client = await getClient();
        await client.query(createCSRFTable);
        await client.query(createTokensTable);
        await client.query(createNotifyFunction);
        await client.query(createNotifyTrigger);
    } catch ( error ) {
        logger.fatal({
            error,
        }, "Failed to migrate tables, exiting microservice");
        process.exit(1);
    } finally {
        if (client) {
            client.release();
        }
    }
    // prepare routes
    const routes = [
        ...authRoutes,
        ...manageRoutes,
    ];
    for (const route of routes) {
        ptts.route(route);
    }
    // start PTTS microservice
    await ptts.start();
    logger.info("micro-service is now listening for connections");
};

const startPubSub = async () => {
    const tokens = [];
    let client: PoolClient|undefined;
    try {
        client = await getClient();
        const foundTokens = await client.query(getTokens);
        tokens.push(...foundTokens.rows);
    } catch (err) {
        logger.error({error: err}, "Failed to fetch the tokens for the pubsub client");
    } finally {
        if (client) { client.release(); }
    }
    const psc = listen(tokens.map((t) => ({
        access_token: t.access_token,
        expiry: t.expiry,
        refresh_token: t.refresh_token,
        user_id: t.id,
    })));
}

const startClient = async () => {
    //
};

(async () => {
    if (process.argv.includes('--server')) {
        await startServer();
    } else if (process.argv.includes('--pubsub')) {
        await startPubSub();
    } else if (process.argv.includes('--client')) {
        await startClient();
    }
})();
